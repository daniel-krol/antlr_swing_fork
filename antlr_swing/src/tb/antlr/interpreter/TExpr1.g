tree grammar TExpr1;

options {
	tokenVocab=Expr;

	ASTLabelType=CommonTree;
    superClass=MyTreeParser;
}

@header {
package tb.antlr.interpreter;
}

prog    : (e=expr {drukuj ($e.text + " = " + $e.out.toString());})* ;

expr returns [Integer out]
	      : ^(PLUS  e1=expr e2=expr) {$out = add($e1.out, $e2.out);}
        | ^(MINUS e1=expr e2=expr) {$out = subtract($e1.out, $e2.out);}
        | ^(MUL   e1=expr e2=expr) {$out = multiply($e1.out, $e2.out);}
        | ^(DIV   e1=expr e2=expr) {$out = divide($e1.out, $e2.out);}
        | ^(MOD   e1=expr e2=expr) {$out = modulo($e1.out, $e2.out);}

        | INT                      {$out = getInt($INT.text);}
        
        // deklarowanie zmiennej o danym identyfikatorze
        | ^(VAR i1=ID)            {declareVariable($i1.text);}
        // ustawianie wartości zmiennej o danym identyfikatorze
        | ^(PODST i1=ID   e2=expr) {setVariableValue($i1.text, $e2.out);}
        // pobieranie wartości zmiennej za pomocą jej identyfikatora
        | ID                       {$out = getVariableValue($ID.text);}
        ;
