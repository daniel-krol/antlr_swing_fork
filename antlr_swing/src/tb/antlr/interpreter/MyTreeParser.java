package tb.antlr.interpreter;

import org.antlr.runtime.*;
import org.antlr.runtime.tree.*;
import tb.antlr.symbolTable.GlobalSymbols;

public class MyTreeParser extends TreeParser {
	
	protected GlobalSymbols globalSymbols = new GlobalSymbols();

    public MyTreeParser(TreeNodeStream input) {
        super(input);
    }

    public MyTreeParser(TreeNodeStream input, RecognizerSharedState state) {
        super(input, state);
    }

    protected void drukuj(String text) {
        System.out.println(text.replace('\r', ' ').replace('\n', ' ').replace("print", ""));
    }

	protected Integer getInt(String text) {
		return Integer.parseInt(text);
	}
	
	protected Integer add(Integer a, Integer b) {
		return a + b;
	}
	
	protected Integer subtract(Integer a, Integer b) {
		return a - b;
	}
	
	protected Integer multiply(Integer a, Integer b) {
		if(b != 0) {
			return a % b;
		}
		else {
			throw new ArithmeticException("modulo by zero: " + a + " % " + b);
		}
	}
	
	protected Integer divide(Integer a, Integer b) {
		if(b != 0) {
			return a / b;
		}
		else {
			throw new ArithmeticException("division by zero: " + a + " / " + b);
		}
	}
	
	protected Integer modulo(Integer a, Integer b) {
		return a % b;
	}
	
	protected void declareVariable(String newSymbolName) {
		if(!globalSymbols.hasSymbol(newSymbolName)) {
			globalSymbols.newSymbol(newSymbolName);
		} else {
			throw new IllegalStateException("Trying to declare an already existing symbol: " + newSymbolName);
		}
	}
	
	protected void setVariableValue(String symbolName, Integer symbolValue) {
		if(globalSymbols.hasSymbol(symbolName)) {
			globalSymbols.setSymbol(symbolName, symbolValue);
		} else {
			throw new IllegalStateException("Trying to assign value: " + symbolValue + " to not existing symbol: " + symbolName);
		}
	}
	
	protected Integer getVariableValue(String symbolName) {
		if(globalSymbols.hasSymbol(symbolName)) {
			return globalSymbols.getSymbol(symbolName);
		} else {
			throw new IllegalStateException("Trying to get value of not declared symbol: " + symbolName);
		}
	}
}
